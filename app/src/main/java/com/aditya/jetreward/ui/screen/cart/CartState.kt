package com.aditya.jetreward.ui.screen.cart

import com.aditya.jetreward.model.OrderReward

data class CartState(
    val orderReward: List<OrderReward>,
    val totalRequiredPoint: Int
)