package com.aditya.jetreward.data

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit


object RewardsApi {
    private const val BASE_URL =
        "http://103.175.216.83/"
    private val retrofit = Retrofit.Builder()
        .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
        .baseUrl(BASE_URL)
        .build()
    val retrofitService : RewardApiService by lazy {
        retrofit.create(RewardApiService::class.java)
    }
}