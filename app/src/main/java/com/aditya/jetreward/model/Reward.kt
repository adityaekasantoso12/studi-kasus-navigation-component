package com.aditya.jetreward.model

import kotlinx.serialization.Serializable
@Serializable
data class Reward(
    val id: Long,
    val image: String,
    val title: String,
    val requiredPoint: Int,
) {
    val imageUrl: String
        get() = "http://103.175.216.83/images/$image"
}
