package com.aditya.jetreward

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import com.aditya.jetreward.di.appModule

class JetRewardAplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@JetRewardAplication)
            modules(appModule)
        }
    }
}