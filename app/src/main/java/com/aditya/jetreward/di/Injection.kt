package com.aditya.jetreward.di

import com.aditya.jetreward.data.RewardRepository


object Injection {
    fun provideRepository(): RewardRepository {
        return RewardRepository.getInstance()
    }
}