package com.aditya.jetreward.di

import com.aditya.jetreward.data.RewardRepository
import com.aditya.jetreward.ui.screen.cart.CartViewModel
import com.aditya.jetreward.ui.screen.detail.DetailRewardViewModel
import com.aditya.jetreward.ui.screen.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { RewardRepository.getInstance() }
    viewModel { HomeViewModel(get()) }
    viewModel { DetailRewardViewModel(get()) }
    viewModel { CartViewModel(get()) }
}
